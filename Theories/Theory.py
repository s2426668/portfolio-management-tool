from abc import ABC, abstractmethod


class Theory(ABC):

    # Generates weights for each item in the portfolio. Utilizes MPMTheory.
    # Param: portfolio items - all potential stocks, no weights
    # Param: risk_preference/return pref - user's preference for risk/return
    # Returns: same portfolio, now with weights. If the stock has 0 weight -either delete, or just set weight to 0
    @abstractmethod
    def generate_portfolio_weights(self, pf_items, return_preference, covariance_all_stocks):
        return pf_items
