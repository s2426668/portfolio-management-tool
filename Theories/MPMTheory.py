from Theories.Theory import Theory
import pandas as pd
from pypfopt.efficient_frontier import EfficientFrontier


class MPMTheory(Theory):
    """method should return the best portfolio, with inputted return rate"""

    def generate_portfolio_weights(self, pf_items, rate_preference, covariance_all_stocks):
        w = self.generate_weights(pf_items, rate_preference, covariance_all_stocks)
        i = 0
        while i < len(pf_items):
            (pf_items[i]).set_proportion(self.get_OrderedDict_val(w, i))
            i = i + 1
        # remove stocks with 0 proportion
        for j in pf_items[:]:
            if j.get_proportion_of_asset() == 0:
                pf_items.remove(j)
        return pf_items

    # using PyPortfolioOpt and its efficient_return
    # the ‘Markowitz portfolio’, minimising volatility for a given target return
    def generate_weights(self, pf_items, rate_preference, covariance_all_stocks):
        i = 0
        retDict = {}
        while i < len(pf_items):
            retDict[(pf_items[i]).get_symbol()] = (pf_items[i]).get_return_rate()
            i = i + 1

        ser = pd.Series(data=retDict, index=list(retDict.keys()))

        EF = EfficientFrontier(ser, covariance_all_stocks, (0, 1),
                               solver=None, verbose=False, solver_options=None)
        weights = EF.efficient_return(rate_preference)
        return weights

    # i do hate OrderedDict, because its 3 am and i cant get access to value (weights)
    def get_OrderedDict_val(self, OrDict, num):
        dictList = list(OrDict.items())
        weightList = dictList[num]
        dictWeight = weightList[1] #wtf numpy.float64, but ok
        normWeight = dictWeight.tolist()

        return normWeight
