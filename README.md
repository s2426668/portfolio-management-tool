# Portfolio management tool

## Installation
1. Install following python packages: pandas, numpy, scikit-learn, pandas_datareader, yahoo_fin, yfinance, PyPortfolioOpt
2. If you are using Mac - update internet certificates.
3. If there is no 'stocks' folder in the portfolio-management-tool create an empty one with that name.

## Usage
1. Run main.py
2. The program will ask you, if it has been already used today. If it has been - then the data for all the stocks is 
downloaded and up to date, hence, it would not be downloaded the second time. If the user enters 'y' when the
data was not downloaded - an error will occur. In that case simply re-run the app.
3. After, the program will ask you for risk-free return rate on the market. It is not advised to enter random numbers,
the rate should be reasonable (example: 0.01).
4. The program will start calculations, and inform you, when it's over. There, go to step 5.
5. The tool will show you the minimum and maximum rate of return you can get, you have to pick something in this range
   (inclusive).
NOTE: if the rate of return selected is outside the range, the closest value in the range will be selected.
6. The tool will display a list of symbols and their proportion in the portfolio required for the least risk with
    the given rate of return.

## Troubleshooting
1. Sometimes, error with data loading might occur. (Due to yahoo finance API). In that case simply re-run the app.
2. If the error with data occurs, try relaunching the app and re-downloading all the data (using 'n' as first input).
3. MAC users only: If the problem with internet certificates occurs, update your internet certificates
   (The tutorial can be found online)
4. WINDOWS users only: If you have problems downloading packages via pip due to "wheel errors"
    or "libxml2/libxslt/lxml not found" - either install libxml2/libxslt (the tutorial can be found online),
    or simply use conda distribution, which already has these pre-installed. The reason for the error is that
    the packages are c++/c libraries, and on windows they need to be installed manually, while on MAC/Linux - 
    they are pre-installed. These are necessary for proper pip functioning, so it is not a fault of our tool.
5. If something else breaks - try waiting some time, and relaunching the app. If still does not work - contact us.
6. If the error with filepath occurs, create a folder "stocks" in the main directory of the tool and relaunch