import math
import sys
import pandas as pd

class Greeting:

    def get_risk_free_rate(self):
        try:
            rate = float(input("Enter risk free rate of return between 0 and 1: "))
            print("Entered risk free rate is - ", rate)
            return rate
        except ValueError:
            print("No.. input is not a number. It's a string")
            self.wrong()

    def get_if_tool_used_today(self):
        answer = input("Hello, have you used tool today? Answer as y/n: ")
        if answer == "n":
            return False
        if answer == "y":
            return True
        else:
            self.wrong()

    def __truncate(self, n, decimals=0):
        multiplier = 10 ** decimals
        return int(n * multiplier) / multiplier

    def get_return_rate(self, max_possible_return_rate, min_possible_return_rate):
        try:
            max_possible_return_rate = self.__truncate(max_possible_return_rate, 2) - 0.01
            min_possible_return_rate = self.__truncate(min_possible_return_rate, 2) + 0.01
            print("Minimum possible rate you can get is: " + str(min_possible_return_rate))
            print("Maximum possible rate you can get is: " + str(max_possible_return_rate))
            rate = float(input("Enter preferable return rate between min and max: "))
            if rate > max_possible_return_rate:
                rate = max_possible_return_rate
            elif rate < min_possible_return_rate:
                rate = min_possible_return_rate
            print("Selected return is - ", rate)
            return rate
        except ValueError:
            print("No.. input is not a number. It's a string")
            self.wrong()


    def wrong(self):
        answerWrong = input("Something is wrong. Would you like to try again? Answer as y/n: ")
        if answerWrong == "y":
            self.get_return_rate()
        if answerWrong == "n":
            print("Goodbye, dear user...")
            sys.exit()
        else:
            self.wrong()

    def printingPortfolio(self, pf):
        pf_items = pf.get_pf_items()
        print('Portfolio weights made via MPT:\n')
        for item in pf_items:
            print(item.get_symbol() + "    " + str(item.get_proportion_of_asset()))

