from Portfolio.asset import Stock


class PortfolioItem:
    __asset = ""
    __proportion = 0.0

    def __init__(self, asset_symbol, r_free, r_market, index_name, proportion=0):
        self.__stock = Stock(asset_symbol, r_free, r_market, index_name)
        self.__proportion = proportion

    def get_asset(self):
        return self.__stock

    def get_proportion_of_asset(self):
        return self.__proportion

    def get_sigma(self):
        return self.get_asset().get_sigma()

    def get_return_rate(self):
        return self.get_asset().get_return_rate()

    def get_symbol(self):
        return self.get_asset().get_symbol()

    def set_proportion(self, weight):
        self.__proportion = weight
