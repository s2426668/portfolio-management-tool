import pandas_datareader
import pandas as pd
from yahoo_fin import stock_info
import yfinance

# Fix for yahoo API
yfinance.pdr_override()

class Screener:
    stocks_folder_name = 'stocks'

    def retrive_all_sp(self): #returns a list of symbols in SP500
        tickers = stock_info.tickers_sp500() #get all S&P 500 names ['AAPL', 'GOOL', ...]
        tickers = [item.replace(".", "-") for item in tickers] # Yahoo Finance uses dashes instead of dots
        return tickers

    def download_ticker_data(self, symbol, start_date, end_date): #downloads csv for one symbol
        df = pandas_datareader.data.get_data_yahoo(symbol, start_date, end_date)
        df.to_csv(self.stocks_folder_name + '/' + f'{symbol}.csv')

# when we filtered
    def retrieve_history_price_data(self, symbols, start_date, end_date):
        data = pandas_datareader.get_data_yahoo(symbols, start_date, end_date)
        return data

    def calculate_covariance(self, price_data_all_stocks):
        monthly_returns = self.calculate_monthly_returns(price_data_all_stocks)
        return monthly_returns.cov()

    def calculate_monthly_returns(self, price_data_all_stocks):
        price_data_adj = price_data_all_stocks['Adj Close']
        monthly_returns = price_data_adj.resample('M').ffill().pct_change()
        return monthly_returns

    def filter(self, price_data, market_return):
        # Calculate moving average for 50, 150 and 200 days
        for i in [50, 150, 200]:
            price_data["SMA_" + str(i)] = round(price_data['Adj Close'].rolling(window=i).mean(), 2)

        # For Mark Minervini's criteria we need following variables.
        current_close = price_data['Adj Close'][-1]
        # 50 days avg
        sma_50 = price_data["SMA_50"][-1]
        sma_150 = price_data["SMA_150"][-1]
        sma_200 = price_data["SMA_200"][-1]
        # 52 weeks high/low price
        low_52wk = round(min(price_data["Low"][-260:]), 2)
        high_52wk = round(max(price_data['High'][-260:]), 2)
        sma_200_20 = price_data['SMA_200'][-20]
        """Mark Minervini's conditions"""
        # Moving average conditions
        cond1 = sma_150 > sma_200
        cond2 = sma_200 > sma_200_20
        cond3 = sma_50 > sma_150 > sma_200
        # Price conditions
        cond4 = current_close > sma_150 > sma_200
        cond5 = current_close > sma_50
        cond6 = current_close >= (low_52wk * 1.3)
        cond7 = current_close >= (0.75 * high_52wk)
        results = [cond1, cond2, cond3, cond4, cond5, cond6, cond7]
        if False in results:
            return False
        return True

    def retrieve_index_return(self, index_name, start_date, end_date):
        index_price_data = pandas_datareader.get_data_yahoo(index_name, start_date, end_date)
        # Save to file
        filepath = self.stocks_folder_name + '/' + f'{index_name}.csv'
        index_price_data.to_csv(filepath)  # write data to file
        index_price_data['Percent'] = index_price_data['Adj Close'].pct_change()
        index_return = (index_price_data['Percent'] + 1).cumprod()[-1] - 1  # Should be between 0 and 1
        return index_return

    def calculate_returns_relative_to_market(self, one_stock_price_data, market_return):
        one_stock_price_data['Change'] = one_stock_price_data['Adj Close'].pct_change()
        r_stock = (one_stock_price_data['Change'] + 1).cumprod()[-1]
        return_multiple = round((r_stock / market_return), 2)
        return return_multiple

    def get_filtered_stocks(self, start, end, market_return_rate, downloaded=False):
        # first we get list of all symbols in SP and then we filter them out
        symbols = self.retrive_all_sp()
        symbols_final = []

        # download all symbols in csv
        if not downloaded:
            for symbol in symbols:
                self.download_ticker_data(symbol, start, end)

        # analyse all csvs one by one and filter them

        # Find stock strength for each one
        relative_stock_returns = []
        for symbol in symbols:
            filepath = self.stocks_folder_name + '/' + f'{symbol}.csv'
            price_data = pd.read_csv(filepath, index_col=0)
            r_to_market_multiple = self.calculate_returns_relative_to_market(price_data, market_return_rate)
            relative_stock_returns.append(r_to_market_multiple)

        # Select only those that have rs > 0.7
        returns = pd.DataFrame(list(zip(symbols, relative_stock_returns)), columns=['Symbol', 'relative_return'])
        returns['Rating'] = returns.relative_return.rank(pct=True) * 100
        returns = returns[returns.Rating >= returns.Rating.quantile(.7)]
        symbols = list(returns['Symbol'])

        for symbol in symbols:
            filepath = self.stocks_folder_name + '/' + f'{symbol}.csv'
            price_data = pd.read_csv(filepath, index_col=0)
            if self.filter(price_data, market_return_rate) == True:  # if the symbol passes filters
                symbols_final.append(symbol)  # this is the final list of symbols after filters
        return symbols_final
