import sys

import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression

class Stock:
    __price = 0
    __return_rate = 0
    __beta = 0
    __sigma = 0
    __free_rate = 0
    __symbol = ""
    __market_rate = 0

    def __init__(self, symbol, free_rate, market_rate, index_symbol):
        self.__symbol = symbol
        self.__free_rate = free_rate
        self.__market_rate = market_rate
        self.__beta, self.__sigma = self.calculate_beta(index_symbol)
        self.__return_rate = self.calculate_return_rate()

    def get_price(self):
        return self.__price

    def get_return_rate(self):
        return self.__return_rate

    def get_beta(self):
        return self.__beta

    def get_sigma(self):
        return self.__sigma

    def get_market_rate(self):
        return self.__market_rate

    def get_symbol(self):
        return self.__symbol

    def get_free_rate(self):
        return self.__free_rate

    # Contacts yahoo.finance, retrieves all the necessary data, calculates sigma, and then - beta. Return: beta, sigma
    def calculate_beta(self, index_symbol):
        symbols = [index_symbol, self.__symbol]
        stocks_folder_name = 'stocks'

        #find the file
        filepath = stocks_folder_name + '/' + f'{symbols[0]}.csv'
        dataSpy = pd.read_csv(filepath, parse_dates=["Date"], index_col=0)

        # get the data of the company from the file
        filepath = stocks_folder_name + '/' + f'{symbols[1]}.csv'
        dataComp = pd.read_csv(filepath)

        # prepare the data for the 2 companies
        price_changeSpy = dataSpy["Adj Close"].pct_change()
        price_changeComp = dataComp["Adj Close"].pct_change()
        cleanedDataSpy = price_changeSpy.drop(price_changeSpy.index[0])  # deletes rows with NaN
        cleanedDataComp = price_changeComp.drop(price_changeComp.index[0])  # deletes rows with NaN

        # SIGMA calculation
        # find the daily log return of the Close price
        # sigmaPriceDay = (np.log(sigmaData / sigmaData.shift(-1)))
        sigmaPriceDay = (np.log(dataComp["Close"] / dataComp["Close"].shift(-1)))
        # calc standart daily deviation
        standDev = np.std(sigmaPriceDay)

        # BETA calculation
        # create numpy arrays with the companies and assign them to a variable
        x = np.array(cleanedDataSpy).reshape(-1, 1)
        y = np.array(cleanedDataComp)
        # Fix for different array sizes. The reason for the bug is the yahoo api, which returns different number of
        # records for the same dates. Hence, we need a workaround. Here we simply delete the extra records.
        if x.size > y.size:
            x = x[:-(x.size - y.size)]
        elif x.size < y.size:
            y = y[:-(y.size - x.size)]
        else:
            pass
        model = LinearRegression().fit(x, y)  # calculate beta using linerear regression
        # calculate volatility(sigma)
        sigma = standDev * x.size ** 0.5
        # the model coefficient will be the beta
        return model.coef_, sigma

    # Uses beta, free_rate and market_rate to calculate return rate utilizing CAPM formula.
    def calculate_return_rate(self):
        """Add implementation here"""
        return_rate = self.get_free_rate() + self.get_beta() * (self.get_market_rate() - self.get_free_rate())
        return return_rate
