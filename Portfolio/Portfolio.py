from Portfolio.PortfolioItem import PortfolioItem


class Portfolio:
    __portfolio_items = []

    def __init__(self, assets_details, free_rate, market_rate, index_name):
        self.__portfolio_items = self.__generate_pf_items(assets_details, free_rate, market_rate, index_name)

    def __generate_pf_items(self, assets_details, free_rate, market_rate, index_name):
        pf_items = []
        for asset in assets_details:
            pf_items.append(PortfolioItem(asset, free_rate, market_rate, index_name))
        return pf_items

    # generates weights for each item in the portfolio. param theory - obj, child of Theory
    def generate_portfolio_weights(self, theory, return_pref, covariance):
        self.__portfolio_items = theory.generate_portfolio_weights(self.get_pf_items(), return_pref, covariance)

    def get_pf_items(self):
        return self.__portfolio_items

    def get_max_possible_return(self):
        return max(self.__get_all_returns_as_list())

    def get_min_possible_return(self):
        return min(self.__get_all_returns_as_list())

    def __get_all_returns_as_list(self):
        returns = []
        for pf_item in self.get_pf_items():
            returns.append(pf_item.get_return_rate())
        return returns
