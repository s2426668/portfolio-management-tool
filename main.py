from Portfolio.Portfolio import Portfolio
from Portfolio.Screener import Screener
from Theories.MPMTheory import MPMTheory
from TUI.Greeting import Greeting

import datetime
import pandas as pd

"""Initial Settings block"""
# Start/end date
start = datetime.datetime.now() - datetime.timedelta(days=366)
end = datetime.datetime.now()

# Index, which represents market return
index_name = 'SPY'
# Path to the covariance csv file.
filepath = 'covariance/cov.csv'

"""Program initialization"""
# Initialize UI class
TUI = Greeting()
# Initialize stock screener
screener = Screener()
# Theory used (is here for possible future extensions for diff theories)
theory = MPMTheory()

"""Input block"""
are_stocks_already_downloaded = TUI.get_if_tool_used_today()  # If data is already downloaded (to speed up the process)
risk_free_rate = TUI.get_risk_free_rate()  # Ask the user for the free rate of return. Should be between 0 and 1

"""Data analysis block"""
# UI changes
if not are_stocks_already_downloaded:
    # Get filtered list of stocks (using Mark Minervini's conditions)
    print("Downloading and Filtering stocks")
else:
    print("Loading and Filtering stocks...")
# market return rate is taken as return from S&P500 index
market_return_rate = screener.retrieve_index_return(index_name, start, end)
final_stocks = screener.get_filtered_stocks(start, end, market_return_rate, are_stocks_already_downloaded)

# covariance optimization
if not are_stocks_already_downloaded:
    # Calculate covariance for the filtered list of stocks (takes the most processing time)
    print("Calculating covariance...")
    cov = screener.calculate_covariance(screener.retrieve_history_price_data(final_stocks, start, end))
    # Write cov to file for the next runs
    cov.to_csv(filepath)
else:
    print("Loading covariance")
    cov = pd.read_csv(filepath, index_col=0)

"""Create portfolio using MPM theory"""
# Weight generation function is invoked automatically
pf = Portfolio(final_stocks, risk_free_rate, market_return_rate, index_name)

# Calculate maximum and minimum possible rate
max_possible_return_rate = pf.get_max_possible_return()
min_possible_return_rate = pf.get_min_possible_return()

# Ask user for return preference. If the return is higher than possible - the max possible will be selected.
rate_pref = TUI.get_return_rate(max_possible_return_rate, min_possible_return_rate)
pf.generate_portfolio_weights(theory, rate_pref, cov)
# Print final result
TUI.printingPortfolio(pf)

